package ru.t1.simanov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public abstract class AbstractException extends RuntimeException {

    public AbstractException(@NotNull String message) {
        super(message);
    }

    public AbstractException(
            @NotNull String message,
            @NotNull Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractException(
            @NotNull String message,
            @NotNull Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
